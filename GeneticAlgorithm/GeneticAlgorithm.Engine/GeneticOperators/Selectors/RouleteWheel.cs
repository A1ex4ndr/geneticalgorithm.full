﻿using GeneticAlgorithm.Interfaces;
using System;
using System.Collections.Generic;

namespace GeneticAlgorithm.Selectors
{
    public class RouleteWheel<TChromosome> : ISelector<TChromosome>
        where TChromosome : class, IChromosome, new()
    {
        private static readonly Random _random = new Random();
        public void Selection(List<TChromosome> chromosomes, int count)
        {
            List<TChromosome> result = new List<TChromosome>();
            int[] array = new int[chromosomes.Count];
            array[0] = (int)chromosomes[0].Fitness;

            for (int i = 1; i < chromosomes.Count; i++)
                array[i] = array[i - 1] + (int)chromosomes[i].Fitness;

            for (int i = 0; i < count; i++)
            {
                var rndValue = _random.Next(0, array[array.Length - 1]);
                var selectedChromosome = chromosomes[BinarySearch(array, rndValue)];
                result.Add(selectedChromosome);
            }
            chromosomes.Clear();
            chromosomes.AddRange(result);
        }
        private int BinarySearch(int[] array, int searchValue)
        {
            int left = 0;
            int right = array.Length;
            while (left <= right)
            {
                int index = (right + left) / 2;
                if (searchValue <= array[index] && (index == 0 || searchValue > array[index - 1]))
                    return index;
                if (searchValue < array[index])
                    right = index;
                if (searchValue > array[index])
                    left = index;
            }
            return -1;
        }
    }
}
