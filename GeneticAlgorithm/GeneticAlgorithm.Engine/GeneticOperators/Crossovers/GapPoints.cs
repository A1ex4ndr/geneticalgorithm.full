﻿using GeneticAlgorithm.Interfaces;
using GeneticAlgorithm.Extensions;
using System;
using System.Linq;

namespace GeneticAlgorithm.Crossovers
{
    public class GapPoints<TChromosome> : ICrossover<TChromosome>
        where TChromosome : class, IChromosome
    {
        private static readonly Random _random = new Random();

        private readonly IChromosomeProvider<TChromosome> _chromosomeProvider;
        public int GapPointsCount { get; set; }

        public GapPoints(IChromosomeProvider<TChromosome> chromosomeProvider)
        {
            _chromosomeProvider = chromosomeProvider;
        }

        public TChromosome Crossover(TChromosome left, TChromosome right)
        {
            int[] rnd = new int[GapPointsCount];
            int part = left.Genotype.Count / (GapPointsCount + 1);
            for (int k = 0; k < GapPointsCount; k++)
                rnd[k] = part * k + _random.Next() % part;
            int[] child = new int[left.Genotype.Count];
            int i = 0;
            for (int k = 0; k < rnd.Length; k++)
                if (k % 2 != 0)
                {
                    for (; i < rnd[k]; i++)
                    {
                        child[i] = left.Genotype[i];
                    }
                }
                else
                {
                    for (; i < rnd[k]; i++)
                    {
                        child[i] = right.Genotype[i];
                    }
                }

            for (; i < left.Genotype.Count(); i++)
                child[i] = left.Genotype[i];
            return _chromosomeProvider.Provide(child.ToSequence());
        }
    }
}
