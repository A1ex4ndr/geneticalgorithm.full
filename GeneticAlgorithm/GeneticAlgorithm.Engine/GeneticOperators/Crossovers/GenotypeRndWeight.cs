﻿using GeneticAlgorithm.Interfaces;
using GeneticAlgorithm.Extensions;
using System;

namespace GeneticAlgorithm.Crossovers
{
    public class GenotypeRndWeight<TChromosome> : ICrossover<TChromosome>
        where TChromosome : class, IChromosome
    {
        private static readonly Random _random = new Random();

        private readonly IChromosomeProvider<TChromosome> _chromosomeProvider;

        public GenotypeRndWeight(IChromosomeProvider<TChromosome> chromosomeProvider)
        {
            _chromosomeProvider = chromosomeProvider;
        }

        public TChromosome Crossover(TChromosome left, TChromosome right)
        {
            if (left.Genotype.Count != right.Genotype.Count)
            {
                throw new ArgumentException("Length of genotypes must be same.");
            }
            int[] child = new int[left.Genotype.Count];
            double p = _random.Next(0, 100);
            for (int i = 0; i < left.Genotype.Count; i++)
            {
                child[i] = (int)(left.Genotype[i] * right.Fitness * p + right.Genotype[i] * left.Fitness * (1 - p));
            }

            return _chromosomeProvider.Provide(child.ToSequence());
        }
    }
}
