﻿using GeneticAlgorithm.Interfaces;
using GeneticAlgorithm.Extensions;
using System;

namespace GeneticAlgorithm.Crossovers
{
    public class GenotypeWeightSum<TChromosome> : ICrossover<TChromosome>
        where TChromosome : class, IChromosome, new()
    {
        private readonly IChromosomeProvider<TChromosome> _chromosomeProvider;

        public GenotypeWeightSum(IChromosomeProvider<TChromosome> chromosomeProvider)
        {
            _chromosomeProvider = chromosomeProvider;
        }

        public TChromosome Crossover(TChromosome first, TChromosome second)
        {
            if (first.Genotype.Count != second.Genotype.Count)
                throw new ArgumentException("Length of genotypes must be same.");

            int[] child = new int[first.Genotype.Count];
            for (int i = 0; i < first.Genotype.Count; i++)
                child[i] = (int)(first.Genotype[i] * 100 / first.Fitness + second.Genotype[i] * 100 / second.Fitness);
         
            return _chromosomeProvider.Provide(child.ToSequence());
        }
    }
}
