﻿using GeneticAlgorithm.Interfaces;
using GeneticAlgorithm.Extensions;
using System;
using System.Linq;

namespace GeneticAlgorithm.Crossovers
{
    public class GenotypeSum<TChromosome> : ICrossover<TChromosome>
        where TChromosome : class, IChromosome
    {
        private readonly IChromosomeProvider<TChromosome> _chromosomeProvider;

        public GenotypeSum(IChromosomeProvider<TChromosome> chromosomeProvider)
        {
            _chromosomeProvider = chromosomeProvider;
        }

        public TChromosome Crossover(TChromosome first, TChromosome second)
        {
            if (first.Genotype.Count != second.Genotype.Count)
                throw new ArgumentException("Length of genotypes must be same.");

            int[] child = new int[first.Genotype.Count];
            for (int i = 0; i < first.Genotype.Count; i++)
                child[i] = first.Genotype[i] + second.Genotype[i];

            return _chromosomeProvider.Provide(child.ToSequence());
        }
    }
}
