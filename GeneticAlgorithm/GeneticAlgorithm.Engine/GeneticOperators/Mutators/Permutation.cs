﻿using GeneticAlgorithm.Interfaces;
using System;

namespace GeneticAlgorithm.Mutators
{
    public class Permutation<TChromosome> : IMutator<TChromosome>
        where TChromosome : class, IChromosome
    {
        protected static readonly Random _random = new Random();
        public void Mutation(TChromosome chromosome)
        {
            int gene1 = _random.Next(0, chromosome.Genotype.Count);
            int gene2;
            do
            {
                gene2 = _random.Next(0, chromosome.Genotype.Count);
            }
            while (gene1 == gene2);

            int tempGene = chromosome.Genotype[gene1];
            
            chromosome.Genotype[gene1] = chromosome.Genotype[gene2];
            chromosome.Genotype[gene2] = tempGene;
        }
    }
}
