﻿using GeneticAlgorithm.Extensions;
using GeneticAlgorithm.Interfaces;
using System;

namespace GeneticAlgorithm.Mutators
{
    public class Replacement<TChromosome> : IMutator<TChromosome>
        where TChromosome : class, IChromosome, new()
    {
        protected static readonly Random _rand = new Random();
        public void Mutation(TChromosome chromosome)
        {
            int geneIndex = _rand.Next(0, chromosome.Genotype.Count - 1);
            chromosome.Genotype[geneIndex] = _rand.Next();
            var mutated = chromosome.Genotype.ToSequence();
            for (int i = 0; i < chromosome.Genotype.Count; i++)
            {
                chromosome.Genotype[i] = mutated[i];
            }
        }
    }
}
