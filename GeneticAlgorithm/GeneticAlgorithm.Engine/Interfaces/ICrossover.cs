﻿namespace GeneticAlgorithm.Interfaces
{
    public interface ICrossover<TChromosome>
        where TChromosome : class, IChromosome
    {
        TChromosome Crossover(TChromosome left, TChromosome right);
    }
}
