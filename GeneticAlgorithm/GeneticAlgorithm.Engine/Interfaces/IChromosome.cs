﻿using System;
using System.Collections.Generic;

namespace GeneticAlgorithm.Interfaces
{
    public interface IChromosome : IComparable
    {
        List<int> Genotype { get; }
        double Fitness { get; }
    }
}
