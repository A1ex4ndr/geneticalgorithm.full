﻿using System.Collections.Generic;

namespace GeneticAlgorithm.Interfaces
{
    public interface IChromosomeProvider<TChromosome> where TChromosome : IChromosome
    {
        TChromosome Provide();
        TChromosome Provide(IEnumerable<int> genotype);
    }
}
