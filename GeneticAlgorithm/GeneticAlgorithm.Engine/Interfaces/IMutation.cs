﻿namespace GeneticAlgorithm.Interfaces
{
    public interface IMutator<TChromosome>
        where TChromosome : class, IChromosome
    {
        void Mutation(TChromosome chromosome);
    }
}
