﻿using GeneticAlgorithm.Presentation.Services.Interfaces;
using System;

namespace GeneticAlgorithm.Presentation.Services
{
    public class AppLogger : IAppLogger
    {
        public event Action<string> OnLogPublished;

        public void Log(string log = default)
        {
            OnLogPublished?.Invoke(log);
            //TODO: write logs to file or database
        }
    }
}
