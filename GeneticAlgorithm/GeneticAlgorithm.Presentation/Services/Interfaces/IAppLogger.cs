﻿using System;

namespace GeneticAlgorithm.Presentation.Services.Interfaces
{
    public interface IAppLogger
    {
        event Action<string> OnLogPublished;
        void Log(string log = default);
    }
}
