﻿using GeneticAlgorithm.Presentation.Pages;
using GeneticAlgorithm.Presentation.Services;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace GeneticAlgorithm.Presentation
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly IServiceProvider _serviceProvider;

        private bool isMinimizedMenu = false;
        private Button activeMenuItem = null;

        public MainWindow()
        {
            _serviceProvider = (Application.Current as Application).ServiceProvider;

            InitializeComponent();

            foreach (UIElement ui in menuBar.Children)
            {
                var asBtn = ui as Button;
                if (asBtn != null)
                    asBtn.Click += MenuItemBtn_Click;
            }
            menuCollapseBtn.Click -= MenuItemBtn_Click;
            MenuItemBtn_Click(settingsBtn, null);
            SettingsBtn_Click(settingsBtn, null);
        }

        private void MenuCollapseButton_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;

            menuIcon.RenderTransformOrigin = new Point(0.5, 0.5);

            TimeSpan duration = TimeSpan.FromSeconds(0.2);

            double angle = 180;
            double trFrom = 45;
            double trTo = 165;
            if (!isMinimizedMenu)
            {
                trFrom = 165;
                trTo = 45;
                angle = 0;
            }

            RotateTransform transform = new RotateTransform()
            {
                CenterX = 0.5,
                CenterY = 0.5,
                Angle = angle + 180,
            };
            menuIcon.RenderTransform = transform;

            DoubleAnimation animation = new DoubleAnimation()
            {
                From = angle,
                To = angle + 180,
                Duration = duration
            };
            DoubleAnimation menuAmination = new DoubleAnimation()
            {
                From = trFrom,
                To = trTo,
                Duration = duration
            };

            ThicknessAnimation thicknessAnimation = new ThicknessAnimation()
            {
                From = new Thickness() { Left = trFrom },
                To = new Thickness() { Left = trTo },
                Duration = duration
            };

            menuBar.BeginAnimation(StackPanel.WidthProperty, menuAmination);
            pageContainer.BeginAnimation(StackPanel.MarginProperty, thicknessAnimation);
            transform.BeginAnimation(RotateTransform.AngleProperty, animation);

            isMinimizedMenu = !isMinimizedMenu;
        }

        private void MenuItemBtn_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;

            if (btn != null)
            {
                if (activeMenuItem != null)
                    activeMenuItem.Background = (new BrushConverter().ConvertFrom("#dddddd") as Brush);

                btn.Background = new BrushConverter().ConvertFrom("#b7b7b7") as Brush;
                activeMenuItem = btn;
            }
        }

        private void PlacingResultBtn_Click(object sender, RoutedEventArgs e)
        {
            Title = "Placing result";
            ChangePage<PlacedShapesPage>();
        }

        private void SettingsBtn_Click(object sender, RoutedEventArgs e)
        {
            Title = "Genetic algorithm settings";
            ChangePage<GeneticEnginePage>();
        }

        private void EngineTraceLog_Click(object sender, RoutedEventArgs e)
        {
            Title = "Trace log";
            ChangePage<EngineLogTracePage>();
        }

        private void ChangePage<TPage>() 
            where TPage : Page
        {
            var page = _serviceProvider.GetService(typeof(TPage)) as TPage;
            if (page is null)
            {
                throw new Exception("Page is not provided.");
            }
            pageContainer.Content = page;
        }
    }
}
