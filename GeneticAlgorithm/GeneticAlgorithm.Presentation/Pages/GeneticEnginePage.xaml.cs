﻿using GeneticAlgorithm.Geometry.Chromosomes;
using GeneticAlgorithm.Geometry.Options;
using GeneticAlgorithm.Geometry.Services.Interfaces;
using GeneticAlgorithm.Interfaces;
using GeneticAlgorithm.Presentation.Controls;
using GeneticAlgorithm.Presentation.Services.Interfaces;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace GeneticAlgorithm.Presentation.Pages
{
    public partial class GeneticEnginePage : Page
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly StripOptions _stripOptions;
        private readonly IShapeService _shapeService;
        private readonly IAppLogger _appLogger;

        private GeneticEngineBuilder<SequenceChromosome> _geneticEngineBuilder = new GeneticEngineBuilder<SequenceChromosome>();
        private GeneticEngine<SequenceChromosome> _engine;
        private CancellationTokenSource _metricCancellation = new CancellationTokenSource();

        public GeneticEnginePage(StripOptions stripOptions,
            IShapeService shapeService,
            IAppLogger appLogger,
            IServiceProvider serviceProvider,
            IChromosomeProvider<SequenceChromosome> chromosomeProvider)
        {
            _serviceProvider = serviceProvider;
            _stripOptions = stripOptions;
            _shapeService = shapeService;
            _appLogger = appLogger;
            _geneticEngineBuilder.UseChromosomeProvider(chromosomeProvider);

            InitializeComponent();

            var allTypes = AppDomain.CurrentDomain.GetAssemblies().SelectMany(asm => asm.GetTypes());

            var gentypes = typeof(GeneticEngine<>).Assembly.GetTypes();

            var selectors = SelectWhereImplementInterface(gentypes, typeof(ISelector<>));
            var crossovers = SelectWhereImplementInterface(gentypes, typeof(ICrossover<>));
            var mutators = SelectWhereImplementInterface(gentypes, typeof(IMutator<>));
            var parentSelectors = SelectWhereImplementInterface(gentypes, typeof(IParentsSelector<>));

            selectorDropdown.DisplayMemberPath = "Value";
            foreach (var item in selectors)
                selectorDropdown.Items.Add(item);

            crossoverDropdown.DisplayMemberPath = "Value";
            foreach (var item in crossovers)
                crossoverDropdown.Items.Add(item);

            mutatorDropdown.DisplayMemberPath = "Value";
            foreach (var item in mutators)
                mutatorDropdown.Items.Add(item);

            parentSelectorDropdown.DisplayMemberPath = "Value";
            foreach (var item in parentSelectors)
                parentSelectorDropdown.Items.Add(item);

            _stripOptions.MaxY = 40;
            ChangeParamGridsEnabledState(true);
        }
        private Dictionary<Type, string> SelectWhereImplementInterface(IEnumerable<Type> source, Type type)
        {
            return source.Where(t => t.IsClass
                    && t.IsGenericType
                    && t.GetGenericTypeDefinition().GetInterfaces()
            .Where(i => i.IsGenericType
                    && i.GetGenericTypeDefinition() == type.GetGenericTypeDefinition()).Count() > 0)
                .ToDictionary(t => t, t => TranslateName(t.Name));
        }
        private void SelectorDropdown_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox comboBox = sender as ComboBox;

            UpdatePropertiesWrapper(comboBox, selectorParamsGrid, selectorParamsWrapper);

            var type = ((KeyValuePair<Type, string>)comboBox.SelectedItem).Key;
            var readyType = type.MakeGenericType(typeof(SequenceChromosome));
            _geneticEngineBuilder.UseSelector(CreateInstance<ISelector<SequenceChromosome>>(readyType));
        }
        private void CrossoverDropdown_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox comboBox = sender as ComboBox;

            UpdatePropertiesWrapper(comboBox, crossoverParamsGrid, crossoverParamsWrapper);

            var type = ((KeyValuePair<Type, string>)comboBox.SelectedItem).Key;
            var readyType = type.MakeGenericType(typeof(SequenceChromosome));
            _geneticEngineBuilder.UseCrossover(CreateInstance<ICrossover<SequenceChromosome>>(readyType));
        }
        private void ParentSelectorDropdown_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox comboBox = sender as ComboBox;

            UpdatePropertiesWrapper(comboBox, parentSelectorParamsGrid, parentSelectorParamsWrapper);
            var type = ((KeyValuePair<Type, string>)comboBox.SelectedItem).Key;
            var readyType = type.MakeGenericType(typeof(SequenceChromosome));
            _geneticEngineBuilder.UseParentsSelector(CreateInstance<IParentsSelector<SequenceChromosome>>(readyType));
        }
        private void MutatorDropdown_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox comboBox = sender as ComboBox;

            UpdatePropertiesWrapper(comboBox, mutatorParamsGrid, mutatorParamsWrapper);
            var type = ((KeyValuePair<Type, string>)comboBox.SelectedItem).Key;
            var readyType = type.MakeGenericType(typeof(SequenceChromosome));
            _geneticEngineBuilder.UseMutator(CreateInstance<IMutator<SequenceChromosome>>(readyType));
        }
        private void UpdatePropertiesWrapper(ComboBox comboBox, Grid grid, WrapPanel wrap)
        {
            if (comboBox == null || comboBox.SelectedItem == null)
            {
                return;
            }
            
            Type type = ((KeyValuePair<Type, string>)comboBox.SelectedItem).Key ?? null;

            if (type == null)
            {
                return;
            }
            grid.Visibility = Visibility.Collapsed;

            PropertyInfo[] properties = type.GetProperties();

            if (properties.Length > 0)
                grid.Visibility = Visibility.Visible;

            wrap.Children.Clear();
            foreach (var prop in properties)
            {
                wrap.Children.Add(new ParameterBox() { Title = prop.Name });
            }
        }
        private string TranslateName(string className)
        {
            int index = className.IndexOf('`');
            if (index != -1)
            {
                className = className.Remove(index);
            }

            for (int i = 1; i < className.Length; i++)
            {
                if (char.IsUpper(className[i]))
                {
                    className = className.Replace(className[i].ToString(), $" {char.ToLower(className[i])}");
                }
            }

            return className;
        }
        private void EngineParamsApplyButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _geneticEngineBuilder.UseBreakCriterionValue(int.Parse(countOfIterationsTextbox.Text))
                    .UseMutationPercent((byte)mutationPercentSlider.Value)
                    .UsePopulationSize(short.Parse(populationSizeTextbox.Text))
                    .UseRemainIndividualsCount(short.Parse(remainCountTextbox.Text));

                _appLogger.Log("Engine parameters changed:");
                _appLogger.Log($"Population size: {_geneticEngineBuilder.PopulationSize}");
                _appLogger.Log($"Remain individuals count: {_geneticEngineBuilder.RemainIndividualsCount}");
                _appLogger.Log($"Count of iterations: {_geneticEngineBuilder.BreakCriterionValue}");
                _appLogger.Log($"Mutation: {_geneticEngineBuilder.MutationPercent}%");
                _appLogger.Log($"Crossover: {TranslateName(nameof(_geneticEngineBuilder.Crossover))}");
                _appLogger.Log($"Selection method: {TranslateName(nameof(_geneticEngineBuilder.Selector))}");
                _appLogger.Log($"Parent selection method: {TranslateName(nameof(_geneticEngineBuilder.ParentsSelector))}");
                _appLogger.Log($"Mutation method: {TranslateName(nameof(_geneticEngineBuilder.Mutator))}");
                _appLogger.Log();
            }
            catch (Exception ex)
            {
                _appLogger.Log($"Exception: {ex.Message}");
                MessageBox.Show(ex.Message, "Exception", MessageBoxButton.OK);
            }
        }
        private void SetPropertyValue(object obj, string propertyName, object value)
        {
            try
            {

                var propInfo = obj.GetType().GetProperty(propertyName);
                propInfo.SetValue(obj, Convert.ChangeType(value, propInfo.PropertyType));
            }
            catch (Exception ex)
            {
                _appLogger.Log($"Exception: {ex.Message}");
                MessageBox.Show(ex.Message, "Exception", MessageBoxButton.OK);
            }
        }
        private void ResetOperatorParameters(UIElementCollection collection)
        {
            try
            {
                foreach (var element in collection)
                {
                    var param = element as ParameterBox;
                    if (!(param is null))
                    {
                        param.ParameterValue = "";
                    }
                }

            }
            catch (Exception ex)
            {
                _appLogger.Log($"ResetOperatorParameters. Exception: {ex.Message}");
                MessageBox.Show(ex.Message, "Exception", MessageBoxButton.OK);
            }
        }
        private void SelectorParamsApplyButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                foreach (var child in selectorParamsWrapper.Children)
                {
                    var param = child as ParameterBox;
                    SetPropertyValue(_geneticEngineBuilder.Selector, param.Title, param.ParameterValue);
                }
            }
            catch (Exception ex)
            {
                _appLogger.Log($"SelectorParamsApply. Exception: {ex.Message}");
                MessageBox.Show(ex.Message, "Exception", MessageBoxButton.OK);
            }
        }
        private void ParentsSelectorParamsApplyButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                foreach (var child in parentSelectorParamsWrapper.Children)
                {
                    var param = child as ParameterBox;
                    SetPropertyValue(_geneticEngineBuilder.ParentsSelector, param.Title, param.ParameterValue);
                }
            }
            catch (Exception ex)
            {
                _appLogger.Log($"ParentsSelectorParamsApply. Exception: {ex.Message}");
                MessageBox.Show(ex.Message, "Exception", MessageBoxButton.OK);
            }
        }
        private void CrossoverParamsApplyButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                foreach (var child in crossoverParamsWrapper.Children)
                {
                    var param = child as ParameterBox;
                    SetPropertyValue(_geneticEngineBuilder.Crossover, param.Title, param.ParameterValue);
                }
            }
            catch (Exception ex)
            {
                _appLogger.Log($"CrossoverParamsApply. Exception: {ex.Message}");
                MessageBox.Show(ex.Message, "Exception", MessageBoxButton.OK);
            }
        }
        private void MutatorParamsApplyButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                foreach (var child in crossoverParamsWrapper.Children)
                {
                    var param = child as ParameterBox;
                    SetPropertyValue(_geneticEngineBuilder.Mutator, param.Title, param.ParameterValue);
                }
            }
            catch (Exception ex)
            {
                _appLogger.Log($"MutatorParamsApply. Exception: {ex.Message}");
                MessageBox.Show(ex.Message, "Exception", MessageBoxButton.OK);
            }
        }
        private void SelectorParamsResetButton_Click(object sender, RoutedEventArgs e)
        {
            ResetOperatorParameters(selectorParamsWrapper.Children);
        }
        private void ParentsSelectorParamsResetButton_Click(object sender, RoutedEventArgs e)
        {
            ResetOperatorParameters(parentSelectorParamsWrapper.Children);
        }
        private void CrossoverParamsResetButton_Click(object sender, RoutedEventArgs e)
        {
            ResetOperatorParameters(crossoverParamsWrapper.Children);
        }
        private void MutatorParamsResetButton_Click(object sender, RoutedEventArgs e)
        {
            ResetOperatorParameters(mutatorParamsWrapper.Children);
        }
        private void EngineParamsResetButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                populationSizeTextbox.Text = "";
                remainCountTextbox.Text = "";
                countOfIterationsTextbox.Text = "";
                mutationPercentSlider.Value = 0;
                crossoverDropdown.SelectedIndex = -1;
                mutatorDropdown.SelectedIndex = -1;
                parentSelectorDropdown.SelectedIndex = -1;
                selectorDropdown.SelectedIndex = -1;

                _appLogger.Log("Engine params reset.");
            }
            catch (Exception ex)
            {
                _appLogger.Log($"Exception: {ex.Message}");
                MessageBox.Show(ex.Message, "Exception", MessageBoxButton.OK);
            }
        }
        private void ChangeParamGridsEnabledState(bool isEnabled)
        {
            engineParamsGrid.IsEnabled = isEnabled;
            selectorParamsGrid.IsEnabled = isEnabled;
            parentSelectorParamsGrid.IsEnabled = isEnabled;
            crossoverParamsGrid.IsEnabled = isEnabled;
            mutatorParamsGrid.IsEnabled = isEnabled;
            chromosomesParameters.IsEnabled = isEnabled;
            runButton.IsEnabled = isEnabled;
            cancelButton.IsEnabled = !isEnabled;
        }
        private async void RunButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _metricCancellation = new CancellationTokenSource();
                ChangeParamGridsEnabledState(false);
                _engine = _geneticEngineBuilder;
                MetricsAsync();
                _appLogger.Log("Genetic algorihm starting.");
                await _engine.ToSolveAsync();
                _metricCancellation.Cancel();
                _appLogger.Log("Genetic algorithm finished.");
                _appLogger.Log($"Elapsed Time: { _engine.ElapsedTime }");
                _appLogger.Log($"Total individuals generated: { _engine.TotalIndividualsGenerated }");
                _appLogger.Log($"Best chromosome fitness: { _engine.Current.Fitness }");
                _appLogger.Log($"Rectangles sequence: { string.Join(" ", _engine.Current.Genotype.ToArray()) }");
                _appLogger.Log($"Total iterations: { _engine.Iteration }");
                _appLogger.Log($"Iteration on which best found: { _engine.IterationOnWhichBestFound }");

                _shapeService.LastResult = _engine.Current;

                ChangeParamGridsEnabledState(true);
            }
            catch (Exception ex)
            {
                CancelButton_Click(null, null);
                _appLogger.Log($"Exception: {ex.Message}");
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK);
            }
        }
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!(_engine is null))
                {
                    _appLogger.Log("Genetic algorithm canceled.");
                    _engine.Cancel();
                }
                if (!(_metricCancellation is null))
                {
                    _metricCancellation.Cancel();
                }
                ChangeParamGridsEnabledState(true);
            }
            catch (Exception ex)
            {
                _appLogger.Log($"Exception: {ex.Message}");
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK);
            }
        }
        private void MetricsAsync()
        {
            Task.Run(() =>
            {
                while (!_metricCancellation.IsCancellationRequested && !(_engine is null))
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        metricsLabel1.Content = $"Current iteration: {_engine.Iteration}\nElapsed time:{_engine.ElapsedTime}";
                    });
                    Thread.Sleep(50);
                }
            });

        }
        private void ChromosomesParamsApplyButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _shapeService.ParseFromString(chromosomesSourceTextBox.Text);
                _appLogger.Log($"Message: Rectangles source applied. Count: {_shapeService.Source.Count}.");
                MessageBox.Show("Source applied.", "Success", MessageBoxButton.OK);
            }
            catch (Exception ex)
            {
                _appLogger.Log($"Source Parsing Exception: {ex.Message}");
                MessageBox.Show($"Wrong data format.", "Parsing error", MessageBoxButton.OK);
            }
        }
        private void ChromosomesFromFileButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string path = null;

                if (ShowFileDialog(out path))
                {
                    _shapeService.ParseFromFile(path);
                }
            }
            catch (Exception ex)
            {
                _appLogger.Log($"Exception: {ex.Message}");
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK);
            }
        }
        private async void ChromosomesParamsSaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string path = null;

                if (ShowFileDialog(out path))
                {
                    using (StreamWriter writer = new StreamWriter(path))
                    {
                        await writer.WriteAsync(chromosomesSourceTextBox.Text);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK);
            }
        }
        private bool ShowFileDialog(out string filePath, string filter = "Text documents (.txt)|*.txt|JSON (.json)|*.json")
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.FileName = string.Empty;
            dlg.DefaultExt = ".json";
            dlg.Filter = filter;

            bool? result = dlg.ShowDialog();

            if (result != null && result == true)
            {
                filePath = dlg.FileName;
                return result.Value;
            }

            filePath = string.Empty;
            return false;
        }
        private void RectHeightApply_Click(object sender, RoutedEventArgs e)
        {
            if (double.TryParse(semiInfiniteRectHeight.Text, out double height))
            {
                _stripOptions.MaxY = height;
            }
            else
            {
                MessageBox.Show("Please input double.", "Error");
            }
        }

        private void mutationPercentSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            mutationPercentLabel.Content = $"Mutation: {(int)e.NewValue}%";
        }

        private T CreateInstance<T>(Type type)
        {
            foreach (var constructor in type.GetConstructors())
            {
                var parameters = constructor.GetParameters();

                try
                {
                    if (parameters.Length == 0)
                    {
                        return (T)Activator.CreateInstance(type);
                    }
                    object[] paramInstances = parameters?.Select(p => _serviceProvider.GetService(p.ParameterType)).ToArray<object>();
                    return (T)Activator.CreateInstance(type, paramInstances);
                }
                catch (Exception)
                {
                    continue;
                }
            }

            throw new InvalidOperationException("No appropriate constructor found.");
        }
    }
}

