﻿using GeneticAlgorithm.Presentation.Services.Interfaces;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;

namespace GeneticAlgorithm.Presentation.Pages
{
    public partial class EngineLogTracePage : Page
    {
        private readonly RichTextBox _textBoxLog;
        private readonly IAppLogger _appLogger;

        public EngineLogTracePage(IAppLogger appLogger)
        {
            _appLogger = appLogger;

            InitializeComponent();
            
            _textBoxLog = new RichTextBox();
            _textBoxLog.Margin = new Thickness() { Bottom = 10, Left = 10, Top = 10, Right = 10};
            gridEngineTraceLog.Children.Add(_textBoxLog);

            _appLogger.OnLogPublished += WriteLog;
        }

        public void WriteLog(string text = "")
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                if (_textBoxLog != null)
                {
                    _textBoxLog.Document.Blocks.Add(new Paragraph(new Run(text)) { LineHeight = 1 });
                }
            });
        }
    }
}
