﻿using GeneticAlgorithm.Geometry.Chromosomes;
using GeneticAlgorithm.Geometry.Options;
using GeneticAlgorithm.Geometry.Services.Interfaces;
using GeneticAlgorithm.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace GeneticAlgorithm.Presentation.Pages
{
    public partial class PlacedShapesPage : Page
    {
        private readonly IChromosomeProvider<SequenceChromosome> _chromosomeProvider;
        private readonly IShapeService _shapeService;
        private readonly StripOptions _stripOptions;

        private Point clickPosition = new Point(0, 0);

        public PlacedShapesPage(IShapeService shapeService, StripOptions stripOptions, IChromosomeProvider<SequenceChromosome> chromosomeProvider)
        {
            _shapeService = shapeService;
            _stripOptions = stripOptions;
            _chromosomeProvider = chromosomeProvider;

            _shapeService.OnPositionsApplied += PlaceShapesAsync;
            InitializeComponent();
        }

        private void DrawMainRect()
        {
            DrawRectangle(_stripOptions.MaxY - _stripOptions.MinY, _shapeService.LastResult?.Fitness ?? 0, _stripOptions.MinX, _stripOptions.MinY, Brushes.Red);
        }

        public void PlaceShapesAsync()
        {
            Task.Run(() =>
            {
                try
                {
                    if (_shapeService.Source is null)
                    {
                        return;
                    }

                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        placingResultCanvas.Children.Clear();

                        foreach (var shape in _shapeService.Source)
                        {
                            var sh = shape.ToShape();
                            sh.Stroke = Brushes.Black;
                            placingResultCanvas.Children.Add(sh);
                            //Canvas.SetLeft(sh, shape.Position?.X ?? 0);
                            //Canvas.SetBottom(sh, shape.Position?.Y ?? 0);
                        }
                        DrawMainRect();
                    });
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Exception", MessageBoxButton.OK);
                }
            });
        }

        private void DrawRectangle(double height, double width, double x, double y, Brush brush = null)
        {
            if (brush == null)
            {
                brush = Brushes.Black;
            }

            var shape = new Rectangle() { Height = height, Width = width, Stroke = brush };
            Canvas.SetLeft(shape, x);
            Canvas.SetBottom(shape, y);
            placingResultCanvas.Children.Add(shape);
        }

        private void PlacingResultCanvas_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            var mousePos = e.GetPosition(placingResultCanvas);

            mousePos.Y = placingResultCanvas.ActualHeight - mousePos.Y;

            foreach (Shape element in placingResultCanvas.Children)
            {
                var left = Canvas.GetLeft(element);
                var bottom = Canvas.GetBottom(element);

                double dragX = (left - mousePos.X) * 0.05 * Math.Sign(e.Delta);
                double dragY = (bottom - mousePos.Y) * 0.05 * Math.Sign(e.Delta);

                Canvas.SetLeft(element, left + dragX);
                Canvas.SetBottom(element, bottom + dragY);

                element.Height += element.Height * 0.05 * Math.Sign(e.Delta);
                element.Width += element.Width * 0.05 * Math.Sign(e.Delta);
            }
        }

        private void PlacingResultCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                var mousePos = e.GetPosition(placingResultCanvas);

                mousePos.Y = placingResultCanvas.ActualHeight - mousePos.Y;

                foreach (Shape element in placingResultCanvas.Children)
                {
                    var left = Canvas.GetLeft(element);
                    var bottom = Canvas.GetBottom(element);
                    Canvas.SetLeft(element, left + (mousePos.X - clickPosition.X));
                    Canvas.SetBottom(element, bottom + (mousePos.Y - clickPosition.Y));
                }

                clickPosition = mousePos;
            }


        }

        private void PlacingResultCanvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            clickPosition = e.GetPosition(placingResultCanvas);
            clickPosition.Y = placingResultCanvas.ActualHeight - clickPosition.Y;
        }



        private int stepPlacingCurrentIndex = 0;

        private void Next_Click(object sender, RoutedEventArgs e)
        {
            if (_shapeService.Source is null || _shapeService.Source.Count == 0)
            {
                MessageBox.Show("Set source.", "Err");
                return;
            }
            if (stepPlacingCurrentIndex == _shapeService.Source.Count)
            {
                stepPlacingCurrentIndex = 0;
                MessageBox.Show("All rects.", "");
                return;
            }

            if (stepPlacingCurrentIndex == 0)
            {
                placingResultCanvas.Children.Clear();
            }

            placingResultCanvas.Children.Add(_shapeService.Source[stepPlacingCurrentIndex].ToShape());
            stepPlacingCurrentIndex++;
        }

        private void SetSequence_Click(object sender, RoutedEventArgs e)
        {
            if (_shapeService.Source == null)
            {
                return;
            }
            List<int> array = sequenceTextBox.Text.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Select(s => int.Parse(s)).ToList();
            if (array.Count != _shapeService.Source.Count)
            {
                return;
            }

            _shapeService.ApplyPositions(array);
        }
    }
}
