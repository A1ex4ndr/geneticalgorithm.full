﻿using GeneticAlgorithm.Geometry.Chromosomes;
using GeneticAlgorithm.Geometry.Interfaces;
using GeneticAlgorithm.Geometry.IntersectionCheckers;
using GeneticAlgorithm.Geometry.Options;
using GeneticAlgorithm.Geometry.Packagers;
using GeneticAlgorithm.Geometry.Providers;
using GeneticAlgorithm.Geometry.Providers.Interfaces;
using GeneticAlgorithm.Geometry.Services;
using GeneticAlgorithm.Geometry.Services.Interfaces;
using GeneticAlgorithm.Geometry.Shapes;
using GeneticAlgorithm.Interfaces;
using GeneticAlgorithm.Presentation.Pages;
using GeneticAlgorithm.Presentation.Services;
using GeneticAlgorithm.Presentation.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace GeneticAlgorithm.Presentation
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class Application : System.Windows.Application
    {
        public IServiceProvider ServiceProvider { get; }

        public Application()
        {
            var services = new ServiceCollection();
            ConfigureServices(services);
            ServiceProvider = services.BuildServiceProvider();
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IIntersectionChecker<Rectangle, Rectangle>, RectangleIntersectionChecker>();
            services.AddSingleton<IIntersectionChecker<Circle, Circle>, CircleIntersectionChecker>();

            services.AddSingleton<IShapesPackager<Rectangle>, RectanglesToStripPackager>();
            services.AddSingleton<IShapesPackager<Circle>, CirclesToStripPackager>();

            services.AddSingleton<IPackagerFactory, PackagerFactory>();
            services.AddSingleton<IChromosomeProvider<SequenceChromosome>, SequenceChromosomeProvider>();
            //services.AddSingleton<IIntersectionCheckerProvider, IntersectionCheckerProvider>();

            services.AddSingleton<IShapeService, ShapeService>();

            services.AddSingleton<EngineLogTracePage>();
            services.AddSingleton<GeneticEnginePage>();
            services.AddSingleton<PlacedShapesPage>();
            
            services.AddSingleton<IAppLogger, AppLogger>();

            services.AddSingleton<StripOptions>();
        }
    }
}
