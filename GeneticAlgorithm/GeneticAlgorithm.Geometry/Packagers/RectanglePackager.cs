﻿using GeneticAlgorithm.Geometry.Interfaces;
using GeneticAlgorithm.Geometry.Options;
using GeneticAlgorithm.Geometry.Shapes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace GeneticAlgorithm.Geometry.Packagers
{
    public class RectanglesToStripPackager : IShapesPackager<Rectangle>
    {
        private readonly IIntersectionChecker<Rectangle, Rectangle> _intersectionChecker;
        private readonly StripOptions _stripOptions;

        public RectanglesToStripPackager(IIntersectionChecker<Rectangle, Rectangle> intersectionChecker, StripOptions stripOptions)
        {
            _intersectionChecker = intersectionChecker;
            _stripOptions = stripOptions;
        }

        public double Pack(List<Rectangle> shapes, IEnumerable<int> sequence, bool applyPositions = false)
        {
            if (sequence is null || sequence.Count() == 0)
            {
                throw new ArgumentException("Sequence is null or empty!");
            }
            if (shapes is null || shapes.Count == 0)
            {
                throw new ArgumentException("Rectangles collection is null or empty!");
            }
            if (sequence.Count() != shapes.Count)
            {
                throw new ArgumentException("Count of rectangles not equal to sequence length!");
            }

            var sortedShapes = new List<Rectangle>();

            for (int i = 0; i < sequence.Count(); i++)
            {
                sortedShapes.Add(shapes[sequence.ElementAt(i)]);
            }

            int placed = 1;
            var positions = new List<Point>();
            positions.Add(new Point(0, 0));

            double fitness = sortedShapes[0].Width;

            for (int i = 1; i < sortedShapes.Count; i++)
            {
                var possiblePositions = new List<Point>();

                for (int j = 0; j < placed; j++)
                {
                    for (int k = j + 1; k < placed; k++)
                    {
                        possiblePositions.AddRange(PlaceRelativeTwoRects(i, j, k, sortedShapes, positions));
                    }
                    possiblePositions.AddRange(PlaceRelativeRectAndBottomBorder(i, j, sortedShapes, positions));
                    possiblePositions.AddRange(PlaceRelativeRectAndLeftBorder(i, j, sortedShapes, positions));
                }
                FilterPossiblePos(i, possiblePositions, positions, sortedShapes, placed);
                Point best = SearchBestPos(sortedShapes, possiblePositions, i, fitness);

                positions.Add(best);
                placed++;

                double max = 0;
                for (int l = 0; l < placed; l++)
                {
                    double tmp = sortedShapes[l].Width + positions[l].X;
                    if (tmp > max)
                    {
                        max = tmp;
                    }
                }
                fitness = max;
            }

            if (applyPositions)
            {
                for (int i = 0; i < sortedShapes.Count; i++)
                {
                    sortedShapes[i].Position = positions[i];
                }
            }

            return fitness;
        }

        public Task<double> PackAsync(List<Rectangle> shapes, IEnumerable<int> sequence, bool applyPositions = false)
        {
            return Task.Run(() => Pack(shapes, sequence, applyPositions));
        }

        private void FilterPossiblePos(int toPlace, List<Point> possiblePos, List<Point> positions, List<Rectangle> rects, int placed)
        {
            for (int i = 0; i < possiblePos.Count; i++)
            {
                // если выходит за границы полубесконечной полосы
                if (
                    (possiblePos[i].X < _stripOptions.MinX)
                 || (possiblePos[i].Y < _stripOptions.MinY)
                 || (possiblePos[i].Y + rects[toPlace].Height > _stripOptions.MaxY)
                )
                {
                    possiblePos.RemoveAt(i);
                    i--;
                    continue;
                }

                for (int j = 0; j < placed; j++) //если пересекается с любым из размещенных прямоугольников
                {
                    if (_intersectionChecker.IsIntersect(rects[toPlace], rects[j], possiblePos[i], positions[j]))
                    {
                        possiblePos.RemoveAt(i);
                        i--;
                        break;
                    }
                }
            }
        }

        private static Point SearchBestPos(List<Rectangle> rects, List<Point> possiblePos, int toPlace, double currentLength)
        {
            double bestLength = currentLength + rects[toPlace].Width;
            Point best = new Point() { X = currentLength, Y = 0 };

            foreach (var p in possiblePos)
            {
                var afterPlacing = p.X + rects[toPlace].Width > currentLength ? p.X + rects[toPlace].Width : currentLength;

                if (afterPlacing == bestLength)
                {
                    return p;
                }
                if (afterPlacing < bestLength)
                {
                    best = p;
                    bestLength = afterPlacing;
                }
            }
            return best;
        }

        private List<Point> PlaceRelativeRectAndLeftBorder(int toPlace, int other, List<Rectangle> rectangles, List<Point> positions)
        {
            List<Point> possible = new List<Point>();

            if (positions[other].X < rectangles[toPlace].Width)
            {
                possible.Add(new Point() { X = _stripOptions.MinX, Y = positions[other].Y + rectangles[other].Height });
            }

            return possible;
        }

        private List<Point> PlaceRelativeRectAndBottomBorder(int toPlace, int other, List<Rectangle> rects, List<Point> positions)
        {
            List<Point> possible = new List<Point>();
            if (positions[other].Y < rects[toPlace].Height)
            {
                possible.Add(new Point() { X = positions[other].X + rects[other].Width, Y = _stripOptions.MinY });
            }
            return possible;
        }

        private static List<Point> PlaceRelativeTwoRects(int toPlace, int first, int second, List<Rectangle> rects, List<Point> positions)
        {
            List<Point> possible = new List<Point>();

            if (positions[first].X <= positions[second].X &&
                positions[second].X < positions[first].X + rects[first].Width + rects[toPlace].Width &&
                positions[second].Y <= positions[first].Y &&
                positions[first].Y < positions[second].Y + rects[second].Height + rects[toPlace].Height &&
                positions[second].X + rects[second].Width > positions[first].X + rects[first].Width
                )
            {
                possible.Add(new Point() { X = positions[first].X + rects[first].Width, Y = positions[second].Y + rects[second].Height });
            }
            if (positions[second].X <= positions[first].X &&
                positions[first].X < positions[second].X + rects[second].Width + rects[toPlace].Width &&
                positions[first].Y <= positions[second].Y &&
                positions[second].Y < positions[first].Y + rects[first].Height + rects[toPlace].Height &&
                positions[second].X + rects[second].Width < positions[first].X + rects[first].Width
                )
            {
                possible.Add(new Point() { X = positions[second].X + rects[second].Width, Y = positions[first].Y + rects[first].Height });
            }

            return possible;
        }

        double IShapesPackager.Pack(List<BaseShape> shapes, IEnumerable<int> sequence, bool applyPositions = false)
        {
            var asRect = shapes
                .Select(shape => shape as Rectangle)
                .ToList();
            return Pack(asRect, sequence, applyPositions);
        }

        Task<double> IShapesPackager.PackAsync(List<BaseShape> shapes, IEnumerable<int> sequence, bool applyPositions = false)
        {
            var asRect = shapes
                .Select(shape => shape as Rectangle)
                .ToList();
            return PackAsync(asRect, sequence, applyPositions);
        }
    }
}
