﻿using GeneticAlgorithm.Geometry.Interfaces;
using GeneticAlgorithm.Geometry.Options;
using GeneticAlgorithm.Geometry.Shapes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace GeneticAlgorithm.Geometry.Packagers
{
    public class CirclesToStripPackager : IShapesPackager<Circle>
    {
        private readonly StripOptions _stripOptions;
        private readonly IIntersectionChecker<Circle, Circle> _intersectionChecker;

        public CirclesToStripPackager(StripOptions stripOptions, IIntersectionChecker<Circle, Circle> intersectionChecker)
        {
            _stripOptions = stripOptions;
            _intersectionChecker = intersectionChecker;
        }

        public double Pack(List<Circle> shapes, IEnumerable<int> sequence, bool applyPositions = false)
        {
            if (sequence is null || sequence.Count() == 0)
            {
                throw new ArgumentException("Sequence is null or empty!");
            }
            if (shapes is null || shapes.Count == 0)
            {
                throw new ArgumentException("Circles collection is null or empty!");
            }
            if (sequence.Count() != shapes.Count)
            {
                throw new ArgumentException("Count of circles not equal to sequence length!");
            }

            var sortedShapes = new List<Circle>();

            for (int i = 0; i < sequence.Count(); i++)
            {
                sortedShapes.Add(shapes[sequence.ElementAt(i)]);
            }

            int placed = 1;
            var positions = new List<Point>();

            positions.Add(new Point(sortedShapes[0].Radius, sortedShapes[0].Radius));

            double fitness = sortedShapes[0].Radius * 2;

            for (int i = 1; i < sortedShapes.Count; i++)
            {
                var possiblePositions = new List<Point>();

                for (int j = 0; j < placed; j++)
                {
                    for (int k = j + 1; k < placed; k++)
                    {
                        possiblePositions.AddRange(PlaceRelativeToTwoCircles(i, j, k, sortedShapes, positions));
                    }
                    possiblePositions.AddRange(PlacementRelativeToCircleAndBottomBorder(i, j, sortedShapes, positions));
                    possiblePositions.AddRange(PlacementRelativeToCircleAndLeftBorder(i, j, sortedShapes, positions));
                    possiblePositions.AddRange(PlacementRelativeToCircleAndTopBorder(i, j, sortedShapes, positions));
                }
                FilterPossiblePositions(i, possiblePositions, positions, sortedShapes, placed);
                Point best = SearchBestPosition(sortedShapes, possiblePositions, i, fitness);

                positions.Add(best);
                placed++;

                double max = 0;
                for (int l = 0; l < placed; l++)
                {
                    double tmp = sortedShapes[l].Radius + positions[l].X;
                    if (tmp > max)
                    {
                        max = tmp;
                    }
                }
                fitness = max;
            }
            if (applyPositions || true)
            {
                for (int i = 0; i < sortedShapes.Count; i++)
                {
                    sortedShapes[i].Position = positions[i];
                }
            }

            return fitness;
        }

        public Task<double> PackAsync(List<Circle> shapes, IEnumerable<int> sequence, bool applyPositions = false)
        {
            return Task.Run(() => Pack(shapes, sequence, applyPositions));
        }

        private void FilterPossiblePositions(int toPlace, List<Point> possiblePos, List<Point> positions, List<Circle> shapes, int placed)
        {
            for (int i = 0; i < possiblePos.Count; i++)
            {
                if (
                    (double.IsNaN(possiblePos[i].X) || double.IsNaN(possiblePos[i].Y))
                 || (possiblePos[i].X - shapes[toPlace].Radius < _stripOptions.MinX - 0.01)
                 || (possiblePos[i].Y - shapes[toPlace].Radius < _stripOptions.MinY - 0.01)
                 || (possiblePos[i].Y + shapes[toPlace].Radius > _stripOptions.MaxY + 0.01)
                )
                {
                    possiblePos.RemoveAt(i);
                    i--;
                    continue;
                }

                for (int j = 0; j < placed; j++)
                {
                    if (_intersectionChecker.IsIntersect(shapes[toPlace], shapes[j], possiblePos[i], positions[j]))
                    {
                        possiblePos.RemoveAt(i);
                        i--;
                        break;
                    }
                }
            }
        }

        private Point SearchBestPosition(List<Circle> shapes, List<Point> possiblePositions, int toPlace, double currentLength)
        {
            double bestLength = currentLength + (2 * shapes[toPlace].Radius);
            Point best = default;
            foreach (var p in possiblePositions)
            {
                var afterPlacing = p.X + shapes[toPlace].Radius > currentLength ? p.X + shapes[toPlace].Radius : currentLength;

                if (afterPlacing == currentLength)
                {
                    return p;
                }
                if (afterPlacing <= bestLength)
                {
                    best = p;
                    bestLength = afterPlacing;
                }
            }
            return best;
        }

        private IEnumerable<Point> PlacementRelativeToCircleAndLeftBorder(int toPlace, int other, List<Circle> shapes, List<Point> positions)
        {
            var edgeA = positions[other].X - _stripOptions.MinX - shapes[toPlace].Radius;
            var edgeB = shapes[other].Radius + shapes[toPlace].Radius;
            var edgeC = Math.Sqrt(Math.Pow(edgeB, 2) - Math.Pow(edgeA, 2));

            if (Math.Abs(edgeA) > edgeB)
            {
                var sign = Math.Sign(edgeA);
                edgeA = Math.Abs(edgeB) * sign;
                edgeC = 0;
            }

            var result = new List<Point>()
            {
                new Point
                {
                    X = positions[other].X - edgeA,
                    Y = positions[other].Y + edgeC
                },
                new Point
                {
                    X = positions[other].X - edgeA,
                    Y = positions[other].Y - edgeC
                }
            };
            return result;
        }

        private IEnumerable<Point> PlacementRelativeToCircleAndTopBorder(int toPlace, int other, List<Circle> shapes, List<Point> positions)
        {
            var edgeA = _stripOptions.MaxY - shapes[toPlace].Radius - positions[other].Y;
            var edgeB = shapes[other].Radius + shapes[toPlace].Radius;
            var edgeC = Math.Sqrt(Math.Pow(edgeB, 2) - Math.Pow(edgeA, 2));

            if (Math.Abs(edgeA) > edgeB)
            {
                var sign = Math.Sign(edgeA);
                edgeA = Math.Abs(edgeB) * sign;
                edgeC = 0;
            }
            var possiblePoint1 = new Point();
            possiblePoint1.X = positions[other].X + edgeC;
            possiblePoint1.Y = positions[other].Y + edgeA;
            var possiblePoint2 = new Point();
            possiblePoint2.X = positions[other].X - edgeC;
            possiblePoint2.Y = positions[other].Y + edgeA;

            var result = new List<Point>();
            result.Add(possiblePoint1);
            if (edgeA != edgeB)
            {
                result.Add(possiblePoint2);
            }

            return result;
        }

        private IEnumerable<Point> PlacementRelativeToCircleAndBottomBorder(int toPlace, int other, List<Circle> shapes, List<Point> positions)
        {
            var edgeA = positions[other].Y - (_stripOptions.MinY + shapes[toPlace].Radius);
            var edgeB = shapes[other].Radius + shapes[toPlace].Radius;
            var edgeC = Math.Sqrt(Math.Pow(edgeB, 2) - Math.Pow(edgeA, 2));

            if (Math.Abs(edgeA) > edgeB)
            {
                var sign = Math.Sign(edgeA);
                edgeA = Math.Abs(edgeB) * sign;
                edgeC = 0;
            }

            var result = new List<Point>()
            {
                new Point
                {
                    X = positions[other].X + edgeC,
                    Y = positions[other].Y - edgeA
                },
                new Point
                {
                    X = positions[other].X - edgeC,
                    Y = positions[other].Y - edgeA
                }
            };

            return result;
        }

        private IEnumerable<Point> PlaceRelativeToTwoCircles(int toPlace, int first, int second, List<Circle> shapes, List<Point> positions)
        {
            double differenceX = positions[first].X - positions[second].X;
            double differenceY = positions[first].Y - positions[second].Y;
            var r1r2 = Math.Sqrt(Math.Pow(differenceX, 2) + Math.Pow(differenceY, 2));
            if (r1r2 >= shapes[first].Radius * 2 + shapes[second].Radius + 2 * shapes[toPlace].Radius)
            {
                return new List<Point>() { new Point() { X = positions[first].X, Y = positions[first].Y } };
            }
            var r1r3 = shapes[first].Radius + shapes[toPlace].Radius;
            var r2r3 = shapes[second].Radius + shapes[toPlace].Radius;
            var semiPerimeter = (r1r2 + r1r3 + r2r3) / 2.0;

            var hr3 = 2 * Math.Sqrt(semiPerimeter * (semiPerimeter - r1r2) * (semiPerimeter - r2r3) * (semiPerimeter - r1r3)) / r1r2;
            var hr2 = Math.Sqrt(Math.Pow(r2r3, 2) - Math.Pow(hr3, 2));

            var H = new Point();
            var lambda = (r1r2 - hr2) / hr2;

            H.X = (positions[first].X + lambda * positions[second].X) / (1 + lambda);
            H.Y = (positions[first].Y + lambda * positions[second].Y) / (1 + lambda);

            var K = new Point() { X = H.X + (positions[second].Y - H.Y), Y = H.Y - (positions[second].X - H.X) };
            var hk = Math.Sqrt(Math.Pow(H.X - K.X, 2) + Math.Pow(H.Y - K.Y, 2));

            lambda = hr3 / hk;

            var vectorHK = new Point() { X = K.X - H.X, Y = K.Y - H.Y };
            vectorHK.X *= lambda;
            vectorHK.Y *= lambda;
            var P3 = new Point();
            P3.X = vectorHK.X + H.X;
            P3.Y = vectorHK.Y + H.Y;
            var P4 = new Point();
            vectorHK.X *= -1;
            vectorHK.Y *= -1;
            P4.X = vectorHK.X + H.X;
            P4.Y = vectorHK.Y + H.Y;
            var result = new List<Point>();
            result.Add(P3);
            result.Add(P4);

            return result;
        }

        double IShapesPackager.Pack(List<BaseShape> shapes, IEnumerable<int> sequence, bool applyPositions = false)
        {
            var asCircle = shapes.Select(shape => shape as Circle).ToList();
            return Pack(asCircle, sequence, applyPositions);
        }

        Task<double> IShapesPackager.PackAsync(List<BaseShape> shapes, IEnumerable<int> sequence, bool applyPositions = false)
        {
            var asCircle = shapes.Select(shape => shape as Circle).ToList();
            return PackAsync(asCircle, sequence, applyPositions);
        }
    }
}
