﻿using System.Windows.Controls;
using System.Windows.Shapes;

namespace GeneticAlgorithm.Geometry.Shapes
{
    public class Rectangle : BaseShape
    {
        public double Height { get; set; }
        public double Width { get; set; }

        public override Shape ToShape()
        {
            var shape = new System.Windows.Shapes.Rectangle()
            {
                Height = Height,
                Width = Width
            };

            if (Position != null)
            {
                Canvas.SetLeft(shape, Position.Value.X);
                Canvas.SetBottom(shape, Position.Value.Y);
            }

            return shape;
        }
    }
}
