﻿using System.Windows.Controls;
using System.Windows.Shapes;

namespace GeneticAlgorithm.Geometry.Shapes
{
    public class Circle : BaseShape
    {
        public double Radius { get; set; }

        public override Shape ToShape()
        {
            double d = Radius * 2;
            var shape = new Ellipse()
            {
                Height = d,
                Width = d
            };
            if (Position != null)
            {
                Canvas.SetLeft(shape, Position.Value.X - Radius);
                Canvas.SetBottom(shape, Position.Value.Y - Radius);
            }

            return shape;
        }
    }
}
