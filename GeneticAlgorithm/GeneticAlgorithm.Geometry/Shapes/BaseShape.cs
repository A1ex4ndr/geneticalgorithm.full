﻿using System.Windows;
using System.Windows.Shapes;

namespace GeneticAlgorithm.Geometry.Shapes
{
    public abstract class BaseShape
    {
        public Point? Position { get; set; }

        public abstract Shape ToShape();
    }
}
