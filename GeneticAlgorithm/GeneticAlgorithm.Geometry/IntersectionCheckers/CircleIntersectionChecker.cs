﻿using GeneticAlgorithm.Geometry.Interfaces;
using GeneticAlgorithm.Geometry.Shapes;
using System;
using System.Windows;

namespace GeneticAlgorithm.Geometry.IntersectionCheckers
{
    public class CircleIntersectionChecker : IIntersectionChecker<Circle, Circle>
    {
        public bool IsIntersect(Circle first, Circle second, Point firstPosition, Point secondPosition)
        {
            double sumRadiuses = first.Radius + second.Radius;
            double distanceBetweenCenters = Math.Sqrt(Math.Pow(firstPosition.X - secondPosition.X, 2) + Math.Pow(firstPosition.Y - secondPosition.Y, 2));

            return sumRadiuses > distanceBetweenCenters + 0.01;
        }
    }
}
