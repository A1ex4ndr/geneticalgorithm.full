﻿using GeneticAlgorithm.Geometry.Interfaces;
using GeneticAlgorithm.Geometry.Shapes;
using System.Windows;

namespace GeneticAlgorithm.Geometry.IntersectionCheckers
{
    public class RectangleIntersectionChecker : IIntersectionChecker<Rectangle, Rectangle>
    {
        public bool IsIntersect(Rectangle first, Rectangle second, Point firstPosition, Point secondPosition)
        {
            return !(firstPosition.X >= secondPosition.X + second.Width
                || firstPosition.X + first.Width <= secondPosition.X
                || firstPosition.Y >= secondPosition.Y + second.Height
                || firstPosition.Y + first.Height <= secondPosition.Y);
        }
    }
}
