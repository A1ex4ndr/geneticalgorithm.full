﻿using GeneticAlgorithm.Geometry.Shapes;
using System.Windows;

namespace GeneticAlgorithm.Geometry.Interfaces
{
    public interface IIntersectionChecker<TFirstShape, TSecondShape>
        where TFirstShape : BaseShape
        where TSecondShape : BaseShape
    {
        bool IsIntersect(TFirstShape first, TSecondShape second, Point firstPosition, Point secondPosition);
    }
}
