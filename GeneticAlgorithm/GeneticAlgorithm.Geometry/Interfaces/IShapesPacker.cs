﻿using GeneticAlgorithm.Geometry.Shapes;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GeneticAlgorithm.Geometry.Interfaces
{
    public interface IShapesPackager
    {
        double Pack(List<BaseShape> shapes, IEnumerable<int> sequence, bool applyPositions = false);
        Task<double> PackAsync(List<BaseShape> shapes, IEnumerable<int> sequence, bool applyPositions = false);
    }

    public interface IShapesPackager<TShape> : IShapesPackager
        where TShape : BaseShape
    {
        double Pack(List<TShape> shapes, IEnumerable<int> sequence, bool applyPositions = false);
        Task<double> PackAsync(List<TShape> shapes, IEnumerable<int> sequence, bool applyPositions = false);
    }


}
