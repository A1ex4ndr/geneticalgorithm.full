﻿using GeneticAlgorithm.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GeneticAlgorithm.Geometry.Chromosomes
{
    public class SequenceChromosome : IChromosome
    {
        public List<int> Genotype { get; }
        public double Fitness { get; }

        public SequenceChromosome()
        {
        }

        public SequenceChromosome(IEnumerable<int> sequence, double fitness)
        {
            Genotype = sequence.ToList();
            Fitness = fitness;
        }

        public int CompareTo(object other)
        {
            SequenceChromosome othr = other as SequenceChromosome;
            if (othr is null)
            {
                throw new InvalidCastException($"RectanglesChromosome.CompareTo: other chromosome is not instance of {nameof(SequenceChromosome)}");
            }

            return Fitness < othr.Fitness
                ? -1
                : Fitness == othr.Fitness
                ? 0
                : 1;
        }
    }
}
