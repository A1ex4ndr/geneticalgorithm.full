﻿namespace GeneticAlgorithm.Geometry.Models
{
    public class ShapeModel
    {
        public string ShapeClass { get; set; }
        public dynamic Shape { get; set; }
    }
}
