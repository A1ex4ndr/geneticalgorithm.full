﻿using GeneticAlgorithm.Geometry.Interfaces;
using GeneticAlgorithm.Geometry.Providers.Interfaces;
using GeneticAlgorithm.Geometry.Shapes;
using System;

namespace GeneticAlgorithm.Geometry.Providers
{
    public class IntersectionCheckerFactory : IIntersectionCheckerFactory
    {
        public IIntersectionChecker<BaseShape, BaseShape> Provide<TFirstShape, TSecondShape>()
            where TFirstShape : BaseShape
            where TSecondShape : BaseShape
        {
            throw new NotImplementedException();
        }

        public IIntersectionChecker<BaseShape, BaseShape> Create(Type firstShapeType, Type secondShapeType)
        {
            throw new NotImplementedException();
        }
    }
}
