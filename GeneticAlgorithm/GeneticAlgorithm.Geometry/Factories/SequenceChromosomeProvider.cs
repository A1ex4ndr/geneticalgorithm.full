﻿using GeneticAlgorithm.Geometry.Chromosomes;
using GeneticAlgorithm.Interfaces;
using GeneticAlgorithm.Geometry.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GeneticAlgorithm.Geometry.Providers
{
    public class SequenceChromosomeProvider : IChromosomeProvider<SequenceChromosome>
    {
        private readonly Random _random;
        private readonly IShapeService _shapeService;

        public SequenceChromosomeProvider(IShapeService shapeService)
        {
            _shapeService = shapeService;
            _random = new Random();
        }

        public SequenceChromosome Provide()
        {
            var sequence = RandomSequence(_shapeService.Source.Count);

            var fitness = _shapeService.ShapesPackager.Pack(_shapeService.Source.ToList(), sequence);
            return new SequenceChromosome(sequence, fitness);
        }

        public SequenceChromosome Provide(IEnumerable<int> genotype)
        {
            var fitness = _shapeService.ShapesPackager.Pack(_shapeService.Source.ToList(), genotype);
            return new SequenceChromosome(genotype, fitness);
        }

        private IEnumerable<int> RandomSequence(int length, int startIndex = 0)
        {
            var sequence = new int[length];
            int indexOfZero = _random.Next(0, length);

            for (int i = 0; i < sequence.Length;)
            {
                if (i == indexOfZero)
                {
                    i++;
                    continue;
                }
                int rnd = _random.Next() % (sequence.Length - 1) + 1;
                if (sequence.Contains(rnd))
                    continue;
                sequence[i++] = rnd;
            }

            return sequence;
        }
    }
}
