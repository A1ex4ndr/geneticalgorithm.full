﻿using GeneticAlgorithm.Geometry.Interfaces;
using GeneticAlgorithm.Geometry.Providers.Interfaces;
using GeneticAlgorithm.Geometry.Shapes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GeneticAlgorithm.Geometry.Providers
{
    public class PackagerFactory : IPackagerFactory
    {
        private readonly IServiceProvider _serviceProvider;

        public PackagerFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public IShapesPackager Create(IEnumerable<BaseShape> shapes)
        {
            if (!shapes.Any())
            {
                throw new ArgumentException("No shapes to be packed.");
            }

            var typesOfShapes = shapes
                .Select(shape => shape.GetType())
                .Distinct()
                .ToList();

            if (typesOfShapes.Count > 1)
            {
                return _serviceProvider.GetService(typeof (IShapesPackager<BaseShape>)) as IShapesPackager<BaseShape>;
            }

            var packagerType = typeof(IShapesPackager<>)
                .MakeGenericType(typesOfShapes.Single());

            return _serviceProvider.GetService(packagerType) as IShapesPackager;
        }
    }
}
