﻿using GeneticAlgorithm.Geometry.Interfaces;
using GeneticAlgorithm.Geometry.Shapes;
using System;

namespace GeneticAlgorithm.Geometry.Providers.Interfaces
{
    public interface IIntersectionCheckerFactory
    {
        IIntersectionChecker<BaseShape, BaseShape> Provide<TFirstShape, TSecondShape>()
            where TFirstShape : BaseShape
            where TSecondShape : BaseShape;
        IIntersectionChecker<BaseShape, BaseShape> Create(Type firstShapeType, Type secondShapeType);
    }
}
