﻿using GeneticAlgorithm.Geometry.Interfaces;
using GeneticAlgorithm.Geometry.Shapes;
using System.Collections.Generic;

namespace GeneticAlgorithm.Geometry.Providers.Interfaces
{
    public interface IPackagerFactory
    {
        IShapesPackager Create(IEnumerable<BaseShape> shapes);
    }
}
