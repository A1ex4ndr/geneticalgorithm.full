﻿using GeneticAlgorithm.Geometry.Chromosomes;
using GeneticAlgorithm.Geometry.Interfaces;
using GeneticAlgorithm.Geometry.Shapes;
using System;
using System.Collections.Generic;

namespace GeneticAlgorithm.Geometry.Services.Interfaces
{
    public interface IShapeService
    {
        event Action OnPositionsApplied;
        IReadOnlyList<BaseShape> Source { get; }
        SequenceChromosome LastResult { get; set; }
        IShapesPackager ShapesPackager { get; }
        void ApplyPositions(IEnumerable<int> sequence);
        void ParseFromFile(string fileFullName);
        void ParseFromString(string shapesString);
    }
}
