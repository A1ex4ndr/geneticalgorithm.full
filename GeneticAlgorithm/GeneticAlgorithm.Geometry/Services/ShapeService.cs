﻿using GeneticAlgorithm.Geometry.Chromosomes;
using GeneticAlgorithm.Geometry.Interfaces;
using GeneticAlgorithm.Geometry.Models;
using GeneticAlgorithm.Geometry.Providers.Interfaces;
using GeneticAlgorithm.Geometry.Services.Interfaces;
using GeneticAlgorithm.Geometry.Shapes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GeneticAlgorithm.Geometry.Services
{
    public class ShapeService : IShapeService
    {
        private readonly IPackagerFactory _packagerProvider;
        private SequenceChromosome _lastResult;

        public IShapesPackager ShapesPackager { get; private set; }
        public IReadOnlyList<BaseShape> Source { get; private set; }
        public SequenceChromosome LastResult
        {
            get
            {
                return _lastResult;
            }
            set
            {
                _lastResult = value;
                ApplyPositions(value.Genotype);
            }
        }

        public ShapeService(IPackagerFactory packagerProvider)
        {
            _packagerProvider = packagerProvider;
        }

        public event Action OnPositionsApplied;

        public void ApplyPositions(IEnumerable<int> sequence)
        {
            var fitness = ShapesPackager.Pack(Source.ToList(), sequence, true);
            _lastResult = new SequenceChromosome(sequence, fitness);
            OnPositionsApplied?.Invoke();
        }

        public void ParseFromFile(string fileFullName)
        {
            var fileText = System.IO.File.ReadAllText(fileFullName);
            DeserializeJson(fileText);
            ShapesPackager = _packagerProvider.Create(Source);
        }

        public void ParseFromString(string shapesString)
        {
            DeserializeJson(shapesString);
            ShapesPackager = _packagerProvider.Create(Source);
        }

        private void DeserializeJson(string json)
        {
            var shapeModels = JsonConvert.DeserializeObject<List<ShapeModel>>(json);

            List<BaseShape> shapes = new List<BaseShape>();
            foreach (var shapeModel in shapeModels)
            {
                switch (shapeModel.ShapeClass)
                {
                    case nameof(Circle):
                        {
                            shapes.Add(new Circle()
                            {
                                Radius = Convert.ToDouble((shapeModel.Shape[nameof(Circle.Radius)] as JValue).Value)
                            });
                        }
                        break;
                    case nameof(Rectangle):
                        {
                            var type = shapeModel.Shape[nameof(Rectangle.Height)].GetType();
                            shapes.Add(new Rectangle()
                            {
                                Height = Convert.ToDouble((shapeModel.Shape[nameof(Rectangle.Height)] as JValue).Value),
                                Width = Convert.ToDouble((shapeModel.Shape[nameof(Rectangle.Width)] as JValue).Value)
                            });
                        }
                        break;

                    default: throw new ArgumentException("Unsupported shape class.");
                }
            }
            Source = shapes;

            ShapesPackager = _packagerProvider.Create(Source);
        }
    }
}
