﻿namespace GeneticAlgorithm.Geometry.Options
{
    public class StripOptions
    {
        public double MinX { get; set; } = 0;
        public double MinY { get; set; } = 0;
        public double MaxX { get; set; } = 0;
        public double MaxY { get; set; } = 0;
    }
}
